using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
namespace SUP.Services;

public class JwtService : IJwtService {
    private readonly IConfiguration _iconf;
    public JwtService(IConfiguration iconf) {_iconf = iconf; }

    public JwtSecurityToken GetNewJwtToken(Claim[] claims, DateTime expDate) {
        SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_iconf["JWT:Key"]!));
        SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        JwtSecurityToken token = new JwtSecurityToken(
            issuer: _iconf["JWT:Issuer"],
            audience: _iconf["JWT:Audience"],
            claims: claims,
            expires: expDate,
            signingCredentials: creds
        );
        return token;
    }
}
