using SUP.Models;
using SUP.Models.DTOs;
using SUP.Exceptions;
using SUP.Repositories;
namespace SUP.Services;

public class SoftwareContractService : ISoftwareContractService {
    private readonly ISoftwareContractRepository _scr;
    private readonly IContractPaymentRepository _cpr;
    private readonly ISoftwareRepository _sr;
    private readonly IDiscountRepository _dr;
    public SoftwareContractService(ISoftwareContractRepository scr, 
            IContractPaymentRepository cpr, ISoftwareRepository sr,
            IDiscountRepository dr) {
        _scr = scr;
        _cpr = cpr;
        _sr = sr;
        _dr = dr;
    }

    private async Task<bool> HasActiveContractWithSoftware(Software soft, int ClientId) {
        SoftwareContract? contract = await _scr.GetContractIfFound(ClientId, soft.ID);
        if (contract != null) {
            ContractPayment payment = await _cpr.GetContractPayment(contract.ID);
            if (payment.FullyPaidOff && 
                    contract.StartDate.AddYears(contract.SoftwareUpdatesYears) < DateTime.Now)
                return true;
        }
        return false; 
    }

    private async Task<bool> ReturningClient(int ClientId) {
        IEnumerable<SoftwareContract>? contracts = await _scr.GetAllClientContracts(ClientId);
        if (contracts != null) {
            foreach (var contract in contracts) {
                if (await _cpr.WasContractSigned(contract))
                    return true;
            }
        }
        return false;
    }

    private async Task<bool> IsContractStillActive(int ContractId) {
        SoftwareContract sc = await _scr.GetContract(ContractId);
        return DateTime.Now <= sc.EndDate;
    }

    public async Task AddNewContract(NewContractDTO newContract) {
        Software soft = await _sr.GetSoftware(newContract.SoftwareId);
        if(await HasActiveContractWithSoftware(soft, newContract.BuyerId))
            throw new DomainException("This buyer already has a active contract on this software");

        int daysBetween = newContract.EndDate.Subtract(newContract.StartDate).Days;
        if (daysBetween < 3 || daysBetween > 30)
            throw new DomainException("Cannot create a contract for a duration less than 3 days or more than 30 days");
        SoftwareContract contract = new SoftwareContract() {
            software = soft,
            BuyerId = newContract.BuyerId,
            StartDate = newContract.StartDate,
            EndDate = newContract.EndDate,
            ContractPrice = soft.YearlyLicencePrice,
            SoftwareUpdateInformation = newContract.SoftwareUpdateInformation,
            SoftwareUpdatesYears = newContract.SoftwareUpdatesYears
        };

        if (newContract.SoftwareUpdatesYears > 1)
            contract.ContractPrice += (decimal)
                (1000*(newContract.SoftwareUpdatesYears-1));

        int totalDiscount = await _dr.GetMaxDiscount(soft);
        if (await ReturningClient(newContract.BuyerId))
            totalDiscount += await _dr.GetReturningCustomerDiscountValue();
        contract.ContractPrice /= totalDiscount;
        
    }

    public async Task<decimal?> AddPaymentToContract(PaymentDTO p) {
        if (!await IsContractStillActive(p.ContractId)) {
            await _cpr.ReturnAllPayments(p.ContractId);            
            await _scr.RemoveContract(p.ContractId);
            throw new DomainException("This contract has expired. You need to create a new one. Return previous payments to the client");
        }
        SoftwareContract contract = await _scr.GetContract(p.ContractId);
        ContractPayment payment = await _cpr.GetContractPayment(p.ContractId);

        decimal? excessAmount = null;
        if (p.Amount+payment.AmountPaid > contract.ContractPrice) {
            excessAmount = contract.ContractPrice-payment.AmountPaid;
            await _cpr.AddPaymnetToContract(p.ContractId, 
                    excessAmount.Value);
        } else
            await _cpr.AddPaymnetToContract(p.ContractId, p.Amount);

        if (payment.AmountPaid == contract.ContractPrice)
            await _cpr.SignContract(payment);
        return excessAmount;
    }
}
