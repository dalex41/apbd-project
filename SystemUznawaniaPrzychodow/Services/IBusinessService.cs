using SUP.Models.DTOs;
namespace SUP.Services;

public interface IBusinessService {
    public Task AddClient(BusinessClientDTO newBusiness);
    public Task UpdateClient(int busClientID, BusinessClientDTO updatedData);
    public Task<decimal> GetIncome(int ID, string? softwareName, string? currency);
    public Task<decimal> GetExpectedIncome(int ID, string? softwareName, string? currency);
}
