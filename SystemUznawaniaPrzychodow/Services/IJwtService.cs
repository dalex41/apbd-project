using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
namespace SUP.Services;

public interface IJwtService {
    public JwtSecurityToken GetNewJwtToken(Claim[] claims, DateTime expDate);
}
