using SUP.Models;
using SUP.Models.DTOs;
using SUP.Repositories;
using SUP.Exceptions;
namespace SUP.Services;

public class IndividualService : IIndividualService {
    private readonly IIndividualRepository _ir;
    public IndividualService(IIndividualRepository ir) {
        _ir = ir;
    }

    private Individual DtoToModel(IndividualClientDTO indDto) {
        return new Individual() {
            FirstName = indDto.FirstName,
            LastName = indDto.LastName,
            Pesel = indDto.Pesel,
            client = new Client() {
                PhoneNumber = indDto.PhoneNumber,
                Email = indDto.PhoneNumber,
                address = new Address() {
                    StreetName = indDto.address.StreetName,
                    BuildingNumber = indDto.address.BuildingnNumber,
                    ApartmentNumnber = indDto.address.ApartmentNumnber,
                    PostalCode = indDto.address.PostalCode,
                    City = indDto.address.City
                }
            }
        };

    }

    public async Task AddClient(IndividualClientDTO newIndividual) {
        Individual ind = DtoToModel(newIndividual);
        await _ir.AddClient(ind);
    }

    public async Task UpdateClient(int indClientID, IndividualClientDTO updatedData) {
        Individual ind = await _ir.GetIndividualById(indClientID);

        if (ind.Pesel != updatedData.Pesel)
            throw new DomainException("Cannot change clients Pesel number!");

        Individual newData = DtoToModel(updatedData);
        newData.ID = newData.ID;
        await _ir.UpdateIndividual(ind, newData);
    }

    public async Task RemoveClient(int ID) {
        Individual ind = await _ir.GetIndividualById(ID);
        await _ir.RemoveIndividual(ind);
    }
}
