using SUP.Models;
using SUP.Models.DTOs;
using SUP.Repositories;
using SUP.Exceptions;
namespace SUP.Services;

public class BusinessService : IBusinessService {
    private readonly IBusinessRepository _br;
    private readonly ISoftwareRepository _sr;
    private readonly ISoftwareContractRepository _scr;
    private readonly IContractPaymentRepository _cpr;
    public BusinessService(IBusinessRepository br, ISoftwareRepository sr,
            ISoftwareContractRepository scr, IContractPaymentRepository cpr) {
        _br = br;
        _sr = sr;
        _scr = scr;
        _cpr = cpr;
    }

    private Business DtoToModel(BusinessClientDTO businessDto) {
        return new Business() {
            Name = businessDto.Name,
            KRSNumber = businessDto.KRSNumber,
            client = new Client() {
                PhoneNumber = businessDto.PhoneNumber,
                Email = businessDto.PhoneNumber,
                address = new Address() {
                    StreetName = businessDto.address.StreetName,
                    BuildingNumber = businessDto.address.BuildingnNumber,
                    ApartmentNumnber = businessDto.address.ApartmentNumnber,
                    PostalCode = businessDto.address.PostalCode,
                    City = businessDto.address.City
                }
            }
        };
    }

    public async Task AddClient(BusinessClientDTO newBusiness) {
        Business bus = DtoToModel(newBusiness);
        await _br.AddClient(bus);
    }

    public async Task UpdateClient(int busClientID, BusinessClientDTO updatedData) {
        Business bus = await _br.GetBusinessById(busClientID);
        if (bus.KRSNumber != updatedData.KRSNumber)
            throw new DomainException("Cannot change clients KRS number!");

        Business newData = DtoToModel(updatedData);
        newData.ID = busClientID;
        await _br.UpdateBusiness(bus, newData);
    }
    //TODO:: Refactor this spaghetti
    private async Task<decimal> CalcIncomePLN(int ID, string? softwareName, bool calcExpectedIncome) {
        decimal income = decimal.Zero;
        IEnumerable<SoftwareContract>? contracts = null!;
        if (softwareName != null) {
            Software software = await _sr.GetSoftware(ID, softwareName);
            contracts = await _scr.GetContractsWithSoftware(software);
        } else {
            IEnumerable<Software>? software = await _sr.GetAllSoftware(ID);
            if (software != null) {
                contracts = new List<SoftwareContract>();
                foreach (Software soft in software) {
                    IEnumerable<SoftwareContract>? cons =
                        await _scr.GetContractsWithSoftware(soft);
                    if (cons != null) {
                        foreach (SoftwareContract sc in cons)
                            contracts.Append(sc);
                    }
                }
            }
        }

        if (contracts != null) {
            foreach (SoftwareContract sc in contracts) {
                if (calcExpectedIncome || await _cpr.WasContractSigned(sc))
                    income += sc.ContractPrice;
            }
        }

        return income; 
    }

    //TODO:: Implement currency conversion
    public async Task<decimal> GetIncome(int ID, string? softwareName, string? currency) {
        decimal IncomePLN = await CalcIncomePLN(ID, softwareName, false);

        return IncomePLN;
    }

    public async Task<decimal> GetExpectedIncome(int ID, string? softwareName, string? currency) {
        decimal IncomePLN = await CalcIncomePLN(ID, softwareName, true);
        return IncomePLN;
    }
}
