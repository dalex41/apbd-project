using SUP.Models.DTOs;
namespace SUP.Services;

public interface IIndividualService {
    public Task AddClient(IndividualClientDTO newIndividual);
    public Task UpdateClient(int indClientID, IndividualClientDTO updatedData);
    public Task RemoveClient(int ID);
}
