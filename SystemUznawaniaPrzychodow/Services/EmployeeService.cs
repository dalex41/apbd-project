using SUP.Models.Auth;
using SUP.Helpers;
using SUP.Repositories;
using SUP.Exceptions;
namespace SUP.Services;

public class EmployeeService : IEmployeeService {
    private readonly IEmployeeRepository _ur;
    public EmployeeService(IEmployeeRepository ur) {
        _ur = ur;
    }

    public async Task AddEmployee(EmpLoginDTO newEmp) {
        Tuple<string, string> pwHashSalt = SecurityHelper.GetHashAndSalt(newEmp.Password);
        Employee emp = new Employee() {
            Login = newEmp.Login,
            PasswordHash = pwHashSalt.Item1,
            Role = newEmp.Role,
            RefreshToken = SecurityHelper.GenerateRefreshToken(),
            RefreshTokenExpiry = DateTime.Now.AddHours(1)
        };
        await _ur.AddEmployee(emp);
    }
    
    private bool VerifyPassword(Employee emp, string enteredPw) {
        return SecurityHelper.GetHashedPasswordWithSalt(enteredPw, emp.Salt)
            .Equals(emp.PasswordHash);
    }

    public async Task<bool> IsAuthorized(string login, string pw) {
        Employee emp = await _ur.GetEmpFromLogin(login);
        return VerifyPassword(emp, pw);
    }

    public async Task SetRefTokenDays(EmpLoginDTO login, string refToken, int expDays) {
        await _ur.SetRefTokenDays(await _ur.GetEmpFromLogin(login.Login),
                refToken, expDays);
    }
}
