using SUP.Models.Auth;
namespace SUP.Services;

public interface IEmployeeService {
    public Task AddEmployee(EmpLoginDTO newEmp);
    public Task<bool> IsAuthorized(string login, string pw);
    public Task SetRefTokenDays(EmpLoginDTO login, string refToken, int expDays);
}
