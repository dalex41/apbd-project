using SUP.Models;
using SUP.Models.DTOs;
namespace SUP.Services;

public interface ISoftwareContractService {
    public Task AddNewContract(NewContractDTO newContract);
    public Task<decimal?> AddPaymentToContract(PaymentDTO payment);
}
