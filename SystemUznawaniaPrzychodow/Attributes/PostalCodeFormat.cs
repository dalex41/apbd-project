using System.ComponentModel.DataAnnotations;
namespace SUP.Attributes;

public class PostalCodeFormat : ValidationAttribute {
    public override bool IsValid(object? value)
        {
            if (value != null)
                return ((string)value).Contains('-');
            return false;
        }
}
