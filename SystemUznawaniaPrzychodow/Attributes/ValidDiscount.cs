using System.ComponentModel.DataAnnotations;
namespace SUP.Attributes;

public class ValidDiscount : ValidationAttribute {
    public override bool IsValid(object? value)
        {
            if (value != null)
                return (int)value > 0 && (int)value < 100;
            return false;
        }
}
