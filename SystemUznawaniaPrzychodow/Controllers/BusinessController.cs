using Microsoft.AspNetCore.Mvc;
using SUP.Models;
using SUP.Models.DTOs;
using SUP.Services;
namespace SUP.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BusinessController : ControllerBase {
    private readonly IBusinessService _bs;
    public BusinessController(IBusinessService bs) {
        _bs = bs; }

    [HttpPost]
    public async Task<IActionResult> AddBusinessClient(BusinessClientDTO newBusiness) {
        await _bs.AddClient(newBusiness);
        return Ok();
    }

    [HttpPut("id")]
    public async Task<IActionResult> UpdateBusinessData(int busClientId, BusinessClientDTO updatedData) {
        await _bs.UpdateClient(busClientId, updatedData);
        return Ok();
    }

    [HttpGet]
    public async Task<IActionResult> GetCurrentIncome(int busClientId, 
            string? softwareName, string? currency) {
        return Ok(await _bs.GetIncome(busClientId, softwareName, currency));
    }

    [HttpGet]
    public async Task<IActionResult> GetExpectedIncome(int busClientId, 
            string? softwareName, string? currency) {
        return Ok(await _bs.GetExpectedIncome(busClientId, softwareName, currency));
    }
}
