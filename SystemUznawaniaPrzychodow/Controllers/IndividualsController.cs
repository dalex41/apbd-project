using Microsoft.AspNetCore.Mvc;
using SUP.Models.DTOs;
using SUP.Services;
namespace SUP.Controllers;

[ApiController]
[Route("api/[controller]")]
public class IndividualsController : ControllerBase {
    private readonly IIndividualService _is;
    public IndividualsController(IIndividualService iis) {
        _is = iis;
    }

    [HttpPost]
    public async Task<IActionResult> AddIndividualClient(IndividualClientDTO newIndividual) {
        await _is.AddClient(newIndividual);
        return Ok();
    }

    [HttpDelete("id")]
    public async Task<IActionResult> RemoveIndividualClient(int id) {
        await _is.RemoveClient(id);
        return Ok();
    }
}
