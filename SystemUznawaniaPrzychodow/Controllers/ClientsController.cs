using Microsoft.AspNetCore.Mvc;
using SUP.Services;
using SUP.Models.DTOs;
namespace SUP.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ClientsController : ControllerBase {
    //private readonly IClientService _cs;
    private readonly ISoftwareContractService _css;
    public ClientsController(ISoftwareContractService css) {
        //_cs = cs; 
        _css = css;
    }

    [HttpPost]
    public async Task<IActionResult> CreateNewContract(NewContractDTO newContract) {
        await _css.AddNewContract(newContract);
        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> AddPayment(PaymentDTO payment) {
        decimal? excess = await _css.AddPaymentToContract(payment);
        if (excess != null)
            return Ok(new { ExcessAmountPaid = excess });
        return Ok();
    }
}
