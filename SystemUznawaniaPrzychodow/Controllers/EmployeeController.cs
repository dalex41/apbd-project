using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using SUP.Services;
using SUP.Helpers;
using SUP.Models.Auth;
namespace SUP.Controllers;

[ApiController]
[Route("api/[controller]")]
public class EmployeeController : ControllerBase {
    private readonly IEmployeeService _es;
    private readonly IJwtService _jwt;
    public EmployeeController(IEmployeeService es, IJwtService jwt) {
        _es = es;
        _jwt = jwt;
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<IActionResult> Register(EmpLoginDTO newLogin) {
        await _es.AddEmployee(newLogin);
        return Ok();
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<IActionResult> Login(EmpLoginDTO userLogin)
    {

        if (!await _es.IsAuthorized(userLogin.Login, userLogin.Password))
        { return Unauthorized("Wrong login or password"); }

        Claim[] claims = new[]
        {
            new Claim(ClaimTypes.Role, userLogin.Role),
        };

        JwtSecurityToken token = _jwt.GetNewJwtToken(claims, DateTime.Now.AddMinutes(30));
    
        string refreshToken = SecurityHelper.GenerateRefreshToken();
        await _es.SetRefTokenDays(userLogin, refreshToken, 1);

        return Ok(new
        {
            accessToken = new JwtSecurityTokenHandler().WriteToken(token),
            refreshToken = refreshToken
        });
    }


}
