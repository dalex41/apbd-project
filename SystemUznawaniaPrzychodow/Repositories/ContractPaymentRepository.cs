using SUP.Models;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
namespace SUP.Repositories;

public class ContractPaymentRepository : IContractPaymentRepository {
    private readonly SupDbContext _ctx;
    public ContractPaymentRepository(SupDbContext ctx) {
        _ctx = ctx;
    }

    public async Task AddPaymentObject(ContractPayment payment) {
        await _ctx.AddAsync(payment);
        await _ctx.SaveChangesAsync();
    }

    public async Task<ContractPayment> GetContractPayment(int ContractId) {
        ContractPayment? payment = await _ctx.ContractPayments
            .Where(p => p.ID == ContractId)
            .SingleOrDefaultAsync();
        if (payment == null)
            throw new DomainException("Contract payment not found");
        return payment;

    }

    public async Task AddPaymnetToContract(int ContractId, decimal AmountPaid) {
        ContractPayment payment = await GetContractPayment(ContractId);
        payment.AmountPaid += AmountPaid;
        PaymentDate pd = new PaymentDate() {
            date = DateTime.Now,
            amount = AmountPaid,
            contractpayment = payment 
        };
        await _ctx.PaymentDates.AddAsync(pd);
        await _ctx.SaveChangesAsync();
    }

    public async Task SignContract(ContractPayment payment) {
        payment.FullyPaidOff = true;
        await _ctx.SaveChangesAsync();
    }

    public async Task<bool> WasContractSigned(SoftwareContract contract) {
        try {
            return await _ctx.ContractPayments.Where(c => c.contract == contract)
            .Select(c => c.FullyPaidOff).SingleOrDefaultAsync();
        } catch (Exception) {
            return false;
        }
    }

    public async Task ReturnAllPayments(int ContractId) {
        ContractPayment p = await GetContractPayment(ContractId);
        p.AmountPaid = 0;
        p.PaymentDates = null!;
        await _ctx.SaveChangesAsync();
    }
}
