using SUP.Models;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
namespace SUP.Repositories;

public class SoftwareContractRepository : ISoftwareContractRepository {
    private readonly SupDbContext _ctx;
    public SoftwareContractRepository(SupDbContext ctx) {
        _ctx = ctx;
    }

    public async Task AddContract(SoftwareContract contract) {
        await _ctx.SoftwareContracts.AddAsync(contract);
        await _ctx.SaveChangesAsync();
    }

    public async Task<SoftwareContract> GetContract(int ID) {
        SoftwareContract? contract = await _ctx.SoftwareContracts.Where(s => s.ID == ID)
            .SingleOrDefaultAsync();
        if (contract == null)
            throw new DomainException("Software contract not found");
        return contract;
    }

    public async Task<SoftwareContract?> GetContractIfFound(int ClientId, int SoftId) {
        SoftwareContract? contract = await _ctx.SoftwareContracts
            .Where(s => s.SoftwareId == SoftId && s.BuyerId == ClientId)
            .SingleOrDefaultAsync();
        return contract;
    }

    public async Task<IEnumerable<SoftwareContract>?> GetAllClientContracts(int ClientId) {
        return await _ctx.SoftwareContracts.Where(c => c.ID == ClientId).ToListAsync();
    }

    public async Task<IEnumerable<SoftwareContract>?> GetContractsWithSoftware(Software software) {
        return await _ctx.SoftwareContracts
            .Where(sc => sc.software.Equals(software)).ToListAsync();
    }

    public async Task RemoveContract(int ID) {
        _ctx.Remove(await GetContract(ID));
        await _ctx.SaveChangesAsync();
    }
}
