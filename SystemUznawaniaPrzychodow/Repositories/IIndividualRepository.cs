using SUP.Models;
namespace SUP.Repositories;

public interface IIndividualRepository {
    public Task AddClient(Individual ind);
    public Task<Individual> GetIndividualById(int ID);
    public Task UpdateIndividual(Individual oldData, Individual newData);
    public Task RemoveIndividual(Individual ind);
}
