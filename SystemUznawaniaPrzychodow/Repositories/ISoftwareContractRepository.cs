using SUP.Models;
namespace SUP.Repositories;

public interface ISoftwareContractRepository {
    public Task AddContract(SoftwareContract contract);
    public Task<SoftwareContract> GetContract(int ID);
    public Task<SoftwareContract?> GetContractIfFound(int ClientId, int SoftId);
    //public Task<bool> HasPreviousSignedContracts(int ClientID);
    //public Task<bool> HasActiveContractOnSoftware(Software software, int ClientID);
    public Task<IEnumerable<SoftwareContract>?> GetAllClientContracts(int ClientId);
    public Task<IEnumerable<SoftwareContract>?> GetContractsWithSoftware(Software software);
    public Task RemoveContract(int ID);
}
