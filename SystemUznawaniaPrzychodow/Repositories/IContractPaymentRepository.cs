using SUP.Models;
namespace SUP.Repositories;

public interface IContractPaymentRepository {
    public Task AddPaymentObject(ContractPayment payment);
    public Task<ContractPayment> GetContractPayment(int ContractId);
    public Task AddPaymnetToContract(int ContractId, decimal AmountPaid);
    public Task SignContract(ContractPayment payment);
    public Task<bool> WasContractSigned(SoftwareContract contract);
    public Task ReturnAllPayments(int ContractId);
}
