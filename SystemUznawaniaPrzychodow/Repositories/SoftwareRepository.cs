using SUP.Models;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
namespace SUP.Repositories;

public class SoftwareRepository : ISoftwareRepository {
    private readonly SupDbContext _ctx;
    public SoftwareRepository(SupDbContext ctx) {
        _ctx = ctx;
    }
    
    public async Task<Software> GetSoftware(int ID) {
        Software? software = await _ctx.Software.Where(s => s.ID == ID)
            .SingleOrDefaultAsync();
        if (software == null)
            throw new DomainException("Software not found");
        return software; 
    }

    public async Task<Software> GetSoftware(int BusinessId, string Name) {
        Software? software = await _ctx.Software
            .Where(s => s.SellerId == BusinessId && s.Name == Name)
            .LastOrDefaultAsync();
        if (software == null)
            throw new DomainException("Software not found");
        return software; 
    }

    public async Task<IEnumerable<Software>?> GetAllSoftware(int BusinessId) {
        return await _ctx.Software.Where(s => s.SellerId == BusinessId).ToListAsync();
    }
}
