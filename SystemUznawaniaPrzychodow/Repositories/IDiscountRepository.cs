using SUP.Models;
namespace SUP.Repositories;

public interface IDiscountRepository {
    public Task<Discount> GetDiscount(int ID);
    public Task<int> GetReturningCustomerDiscountValue();
    public Task<int> GetMaxDiscount(Software software);
}
