using SUP.Models;
namespace SUP.Repositories;

public interface IBusinessRepository {
    public Task AddClient(Business bus);
    public Task<Business> GetBusinessById(int ID);
    public Task UpdateBusiness(Business oldData, Business newData);
}
