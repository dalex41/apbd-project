using SUP.Models.Auth;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
namespace SUP.Repositories;

public class EmployeeRepository : IEmployeeRepository {
    private readonly SupDbContext _ctx;
    public EmployeeRepository(SupDbContext ctx) {
        _ctx = ctx;
    }

    public async Task AddEmployee(Employee emp) {
        await _ctx.AddAsync(emp);
        await _ctx.SaveChangesAsync();

    }
    public async Task<Employee> GetEmpFromLogin(string login) {
        Employee? emp = 
        await _ctx.Employees.Where(u => u.Login.Equals(login))
            .SingleOrDefaultAsync();
        if (emp == null)
            throw new DomainException("Login not found");
        return emp;
    }

    public async Task<Employee> GetEmpFromRefToken(string refToken) {
        Employee? emp = await _ctx.Employees.Where(u => u.RefreshToken.Equals(refToken))
            .SingleOrDefaultAsync();
        if (emp == null)
            throw new SecurityTokenException("No refreshToken "+refToken+" found");
        return emp;
    }

    public async Task<Employee> GetEmpByValidRefToken(string refToken) {
        Employee emp = await GetEmpFromRefToken(refToken);
         if (emp.RefreshTokenExpiry < DateTime.Now)
            throw new SecurityTokenException("Refresh token has expired");
         return emp;
    }

    public async Task SetRefTokenDays(Employee emp, string refToken, int expDays) {
        emp.RefreshToken = refToken;
        emp.RefreshTokenExpiry = DateTime.Now.AddDays(expDays);
        await _ctx.SaveChangesAsync();
    }

}
