using SUP.Models;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
namespace SUP.Repositories;

public class IndividualRepository : IIndividualRepository {
    private readonly SupDbContext _ctx;
    public IndividualRepository(SupDbContext ctx) {
        _ctx = ctx;
    }

    public async Task AddClient(Individual individual) {
        await _ctx.Clients.AddAsync(individual.client);
        await _ctx.IndividualClients.AddAsync(individual);
        await _ctx.SaveChangesAsync();
        
    }

    public async Task<Individual> GetIndividualById(int ID) {
        Individual? ind =  await _ctx.IndividualClients.Where(b => b.ID == ID)
            .SingleOrDefaultAsync();
        if (ind == null)
            throw new DomainException("Business client not found");
        return ind;

    }

    public async Task UpdateIndividual(Individual oldData, Individual newData) {

        //oldData.client previous entry stays in db
        oldData.client = newData.client;
        oldData.FirstName = newData.FirstName;
        oldData.LastName = newData.LastName;
        await _ctx.SaveChangesAsync();
    }

    public async Task RemoveIndividual(Individual ind) {
        _ctx.IndividualClients.Remove(ind);
        await _ctx.SaveChangesAsync();
    }
}
