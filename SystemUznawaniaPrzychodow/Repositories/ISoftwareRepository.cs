using SUP.Models;
namespace SUP.Repositories;

public interface ISoftwareRepository {
    public Task<Software> GetSoftware(int ID);
    public Task<Software> GetSoftware(int BusinessId, string Name);
    public Task<IEnumerable<Software>?> GetAllSoftware(int BusinessId);
}
