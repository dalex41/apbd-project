using SUP.Models;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
namespace SUP.Repositories;

public class DiscountRepository : IDiscountRepository {
    private readonly SupDbContext _ctx;
    public DiscountRepository(SupDbContext ctx) {
        _ctx = ctx;
    }

    public async Task<Discount> GetDiscount(int ID) {
        Discount? discount = await _ctx.Discounts.Where(d => d.ID == ID)
            .SingleOrDefaultAsync();
        if (discount == null)
            throw new DomainException("Discount not found");
        return discount;

    }

    public async Task<int> GetReturningCustomerDiscountValue() {
        Discount discount = await _ctx.Discounts.Where(d => 
                d.Name.Equals("Persistent client discount"))
            .SingleAsync();
        return discount.DiscountPercentage;
    }
    
    public async Task<int> GetMaxDiscount(Software software) {
        int? maxDiscount = null;
        try {
            maxDiscount = await _ctx.Discounts.Where(d => d.software.Contains(software)
                    && d.StartingFrom <= DateTime.Now && d.EndingAt >= DateTime.Now)
                .Select(d => d.DiscountPercentage).MaxAsync();
        } catch (Exception) {
            maxDiscount = 0;
        }
        return maxDiscount.Value;

    }
}
