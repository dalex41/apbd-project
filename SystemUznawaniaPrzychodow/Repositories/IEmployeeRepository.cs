using SUP.Models.Auth;
namespace SUP.Repositories;

public interface IEmployeeRepository {
    public Task AddEmployee(Employee emp);
    public Task<Employee> GetEmpFromLogin(string login);
    public Task<Employee> GetEmpFromRefToken(string refToken);
    public Task<Employee> GetEmpByValidRefToken(string refToken);
    public Task SetRefTokenDays(Employee emp, string refToken, int expDays);
}
