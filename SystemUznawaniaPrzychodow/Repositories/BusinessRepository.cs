using SUP.Models;
using SUP.Data;
using SUP.Exceptions;
using Microsoft.EntityFrameworkCore;
namespace SUP.Repositories;

public class BusinessRepository : IBusinessRepository {
    private readonly SupDbContext _ctx;
    public BusinessRepository(SupDbContext ctx) {
        _ctx = ctx;
    }

    public async Task AddClient(Business business) {
        await _ctx.Clients.AddAsync(business.client);
        await _ctx.BusinessClients.AddAsync(business);
        await _ctx.SaveChangesAsync();
    }

    public async Task<Business> GetBusinessById(int ID) {
        Business? bus =  await _ctx.BusinessClients.Where(b => b.ID == ID)
            .SingleOrDefaultAsync();
        if (bus == null)
            throw new DomainException("Business client not found");
        return bus;

    }

    public async Task UpdateBusiness(Business oldData, Business newData) {
        
        //oldData.client previous entry stays in db
        oldData.client = newData.client;
        oldData.Name = newData.Name;
        await _ctx.SaveChangesAsync();
    }

    public async Task<Client> GetClient(int BusinessId) {
        return await _ctx.BusinessClients.Where(b => b.ID == BusinessId)
            .Select(b => b.client).SingleOrDefaultAsync();
    }
}
