using Microsoft.AspNetCore.Http;
using System;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using SUP.Services;
namespace SUP.Middlewares;

public class BasicAuthMiddleware {
    private readonly RequestDelegate _nextMw;
    private readonly IEmployeeService _srv;
    private const string Realm = "Employee realm"; 

    public BasicAuthMiddleware(RequestDelegate nextMw, 
            IEmployeeService srv) {
        _nextMw = nextMw;
        _srv = srv;
    }

    public async Task InvokeAsync(HttpContext context) {
        if (context.Request.Headers.ContainsKey("Authorization"))
        {
            var authHeader = AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"]);

            if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) &&
                authHeader.Parameter != null)
            {
                var credentials = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader.Parameter)).Split(':');
                string username = credentials[0];
                string password = credentials[1];

                // Replace with your own user validation logic
                if (await _srv.IsAuthorized(username, password))
                {
                    await _nextMw(context);
                    return;
                }
            }
        }

        // Return 401 Unauthorized if authentication fails
        context.Response.Headers["WWW-Authenticate"] = $"Basic realm=\"{Realm}\"";
        context.Response.StatusCode = StatusCodes.Status401Unauthorized;


    }
}

