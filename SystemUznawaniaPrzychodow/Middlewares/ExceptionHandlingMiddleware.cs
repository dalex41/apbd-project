using System.Net;
namespace SUP.Middlewares;

public class ExceptionHandlingMiddleware {
    private readonly RequestDelegate _next;
    public ExceptionHandlingMiddleware(RequestDelegate next) {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context) {
        try {
            await _next(context);
        } catch (Exception ex){ 
            await HandleException(context, ex);
        }
    }

    private Task HandleException(HttpContext context, Exception ex) {
        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
        context.Response.ContentType = "application/json";

        var httpResponse = new {
            Message = "Error occured while trying to process your request.",
            Details = ex.Message,
        };
        return context.Response.WriteAsync(
                System.Text.Json.JsonSerializer.Serialize(httpResponse));
    }


}
