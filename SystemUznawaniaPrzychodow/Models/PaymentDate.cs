using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

public class PaymentDate {
    [Key]
    public int ID {get; set; }
    public DateTime date {get; set; }
    [Column(TypeName = "decimal(7, 2)")]
    public decimal amount {get; set; }
    public int ContractPaymentID {get; set; }
    [ForeignKey(nameof(ContractPaymentID))]
    public ContractPayment contractpayment {get; set; } = null!;
}
