using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
namespace SUP.Models.DTOs;

public class PaymentDTO {
    [Required]
    public int ContractId {get; set; }
    [Required]
    [Precision(7,2)]
    public decimal Amount {get; set; }
}
