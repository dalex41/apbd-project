using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SUP.Models;
using SUP.Attributes;
namespace SUP.Models.DTOs;

public class NewContractDTO {
    [Required]
    public int SoftwareId {get; set; }
    [Required]
    public int BuyerId {get; set; }
    [Required]
    public DateTime StartDate {get; set; }
    [Required]
    public DateTime EndDate {get; set; }
    [Required]
    public decimal ContractPrice {get; set; }
    [MaxLength(150)]
    public string SoftwareUpdateInformation {get; set; } = null!;
    [Range(1, 4)]
    public int SoftwareUpdatesYears {get; set; } = 1;

}
