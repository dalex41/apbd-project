using System.ComponentModel.DataAnnotations;
namespace SUP.Models.DTOs;

public class BusinessClientDTO {
    [Required]
    [MaxLength(100)]
    public string Name {get; set; } = null!;
    [Required]
    [Phone]
    [MinLength(9)]
    [MaxLength(9)]
    public string PhoneNumber { get; set; } = null!;
    [Required]
    [EmailAddress]
    [MaxLength(100)]
    public string Email { get; set; } = null!;
    [Required]
    public AddressDTO address { get; set; } = null!;
    [Required]
    [MinLength(12)]
    [MaxLength(12)]
    public string KRSNumber {get; set; } = null!;
}
