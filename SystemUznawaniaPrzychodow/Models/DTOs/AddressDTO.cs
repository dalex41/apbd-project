using System.ComponentModel.DataAnnotations;
using SUP.Attributes;
namespace SUP.Models.DTOs;


public class AddressDTO {
    [Required]
    [MaxLength(150)]
    public string StreetName { get; set; } = null!;
    [Required]
    [MaxLength(5)]
    public string BuildingnNumber { get; set; } = null!;
    [MaxLength(5)]
    public string ApartmentNumnber { get; set; } = null!;
    [Required]
    [PostalCodeFormat]
    [MinLength(6)]
    [MaxLength(6)]
    public string PostalCode { get; set; } = null!;
    [Required]
    [MaxLength(100)]
    public string City { get; set; } = null!;
}
