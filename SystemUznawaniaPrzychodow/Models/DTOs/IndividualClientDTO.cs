using System.ComponentModel.DataAnnotations;
namespace SUP.Models.DTOs;

public class IndividualClientDTO {
    [Required]
    [MaxLength(50)]
    public string FirstName { get; set; } = null!;
    [Required]
    [MaxLength(50)]
    public string LastName { get; set; } = null!;
    [Required]
    [Phone]
    [MinLength(9)]
    [MaxLength(9)]
    public string PhoneNumber { get; set; } = null!;
    [Required]
    [EmailAddress]
    [MaxLength(100)]
    public string Email { get; set; } = null!;
    [Required]
    public AddressDTO address { get; set; } = null!;
    [Required]
    [MinLength(11)]
    [MaxLength(11)]
    public string Pesel { get; set; } = null!;
}
