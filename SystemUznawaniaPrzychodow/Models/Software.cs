using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

/*
public enum SoftwareCategory {
    Finance, Education, Entertainment
}
*/

public class Software {
    [Key]
    public int ID {get; set; }
    [MaxLength(150)]
    public string Name {get; set; } = null!;
    [MaxLength(150)]
    public string Description {get; set; } = null!;
    [MaxLength(50)]
    public string CurrentVersion {get; set; } = null!;
    [MaxLength(50)]
    public string Category {get; set; } = null!;
    [Column(TypeName = "decimal(7, 2)")]
    public decimal YearlyLicencePrice {get; set; }
    public DateTime ReleaseDate {get; set; }
    
    public int SellerId {get; set; }
    [ForeignKey(nameof(SellerId))]
    public Client seller {get; set; } = null!;

    public IEnumerable<SoftwareContract?> contracts = null!;
    public IEnumerable<Discount?> discounts {get; set; } = null!;
}
