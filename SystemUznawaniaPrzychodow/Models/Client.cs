using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

public class Client {
    [Key]
    public int ID {get; set; }
    public int AddressID { get; set; }
    //[Phone]
    [MaxLength(9)]
    public string PhoneNumber { get; set; } = null!;
    //[EmailAddress]
    [MaxLength(100)]
    public string Email { get; set; } = null!;
    [ForeignKey(nameof(AddressID))]
    public Address address { get; set; } = null!;

    public Individual? individual { get; set; } = null!;
    public Business? business { get; set; } = null!;
    public IEnumerable<Software?> software {get; set; } = null!;
    public IEnumerable<SoftwareContract?> contracts {get; set; } = null!;
    
}
