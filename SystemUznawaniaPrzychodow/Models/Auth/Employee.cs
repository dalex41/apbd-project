using System.ComponentModel.DataAnnotations;
namespace SUP.Models.Auth;

public class Employee {
    [Key]
    public int ID {get; set; }
    public string Login { get; set; } = null!;
    public string PasswordHash { get; set; } = null!;
    public string Role {get; set; } = null!;
    public string Salt { get; set; } = null!;
    public string RefreshToken { get; set; } = null!;
    public DateTime? RefreshTokenExpiry { get; set; } = null!;
}

public class EmpLoginDTO {
    [Required]
    public string Login {get; set; } = null!;
    [Required]
    public string Password {get; set; } = null!;
    [Required]
    public string Role {get; set; } = null!;
}
