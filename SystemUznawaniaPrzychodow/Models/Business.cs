using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

public class Business {
    [Key]
    public int ID { get; set; }
    public int ClientID {get; set; }
    [MaxLength(100)]
    public string Name {get; set; } = null!;
    [MaxLength(12)]
    public string KRSNumber {get; set; } = null!;

    [ForeignKey(nameof(ClientID))]
    public Client client {get; set; } = null!;
}
