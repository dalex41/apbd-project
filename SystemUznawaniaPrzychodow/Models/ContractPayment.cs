using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

public class ContractPayment {
    [Key]
    public int ID {get; set; }
    public int ContractId {get; set; }
    [ForeignKey(nameof(ContractId))]
    public SoftwareContract contract {get; set; } = null!;

    [Column(TypeName = "decimal(7, 2)")]
    public decimal AmountPaid {get; set; } = decimal.Zero;
    public bool FullyPaidOff {get; set; } = false;
    public IEnumerable<PaymentDate?> PaymentDates {get; set; } = null!;
}
