using System.ComponentModel.DataAnnotations;
using SUP.Attributes;
namespace SUP.Models;


public class Address {
    [Key]
    public int ID { get; set; }
    [Required]
    [MaxLength(150)]
    public string StreetName { get; set; } = null!;
    [Required]
    [MaxLength(5)]
    public string BuildingNumber { get; set; } = null!;
    [MaxLength(5)]
    public string ApartmentNumnber { get; set; } = null!;
    [Required]
    [PostalCodeFormat]
    [MinLength(6)]
    [MaxLength(6)]
    public string PostalCode { get; set; } = null!;
    [Required]
    [MaxLength(100)]
    public string City { get; set; } = null!;

    public IEnumerable<Client>? clients = null!;

}
