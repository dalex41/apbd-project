using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

public class Individual {
    [Key]
    public int ID {get; set; }
    public int? ClientID {get; set; }
    [MaxLength(50)]
    public string FirstName { get; set; } = null!;
    [MaxLength(50)]
    public string LastName { get; set; } = null!;
    [MaxLength(11)]
    public string Pesel { get; set; } = null!;

    [ForeignKey(nameof(ClientID))]
    public Client? client {get; set; } = null!;
}
