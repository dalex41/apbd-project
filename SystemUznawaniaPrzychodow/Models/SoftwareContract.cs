using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SUP.Models;

public class SoftwareContract {
    [Key]
    public int ID {get; set; }
    public int SoftwareId {get; set; }
    [ForeignKey(nameof(SoftwareId))]
    public Software software {get; set; } = null!;
    //Not sure if i should've added support for multiple softwares in a contract
    //public IEnumerable<Software> software {get; set; } = null!;
    public int BuyerId {get; set; }
    [ForeignKey(nameof(BuyerId))]
    public Client client {get; set; } = null!;

    public DateTime StartDate {get; set; }
    public DateTime EndDate {get; set; }
    [Column(TypeName = "decimal(7, 2)")]
    public decimal ContractPrice {get; set; }
    [MaxLength(150)]
    public string SoftwareUpdateInformation {get; set; } = null!;
    //[Range(1, 4)]
    public int SoftwareUpdatesYears {get; set; } = 1;
}
