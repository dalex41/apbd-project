using System.ComponentModel.DataAnnotations;
using SUP.Attributes;
namespace SUP.Models;

public class Discount {
    [Key]
    public int ID {get; set;}
    [MaxLength(150)]
    public string Name {get; set; } = null!;
    [MaxLength(150)]
    public string Descrption {get; set; } = null!;
    [ValidDiscount]
    public int DiscountPercentage;
    public DateTime? StartingFrom {get; set; }
    public DateTime? EndingAt {get; set; }
    
    //May need to change how sale logic applies to software offers
    public IEnumerable<Software?> software {get; set; } = null!;
}
