using Microsoft.EntityFrameworkCore;
using SUP.Models;
using SUP.Models.Auth;
namespace SUP.Data;

public partial class SupDbContext : DbContext {
    public SupDbContext() {}

    public SupDbContext(DbContextOptions<SupDbContext> options)
        : base(options) {}

    public DbSet<Client> Clients { get; set; }
    public DbSet<Individual> IndividualClients { get; set; }
    public DbSet<Business> BusinessClients { get; set; }
    public DbSet<Software> Software {get; set; }
    public DbSet<Discount> Discounts {get; set; }
    public DbSet<SoftwareContract> SoftwareContracts {get; set; }
    public DbSet<ContractPayment> ContractPayments {get; set; }
    public DbSet<PaymentDate> PaymentDates {get; set; }
    public DbSet<Employee> Employees {get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Individual>(i => 
                i.HasOne(i => i.client)
                .WithOne(c => c.individual)
                .OnDelete(DeleteBehavior.NoAction));
        modelBuilder.Entity<Discount>(d =>
                d.HasData(new Discount() {
                    ID = 1,
                    Name = "Persistent client discount",
                    Descrption = "Discount for all clients that have previously signed a contract",
                    DiscountPercentage = 5 }));
        modelBuilder.Entity<SoftwareContract>(s => {
                s.HasOne(c => c.software)
                .WithMany(soft => soft.contracts)
                .OnDelete(DeleteBehavior.NoAction);
                s.HasOne(c => c.client)
                .WithMany(client => client.contracts)
                .OnDelete(DeleteBehavior.NoAction); });
    }
}
