﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace apbd_project.Migrations
{
    /// <inheritdoc />
    public partial class UpdatedContractPayments : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullPaymnetDate",
                table: "ContractPayments");

            migrationBuilder.AddColumn<bool>(
                name: "FullyPaidOff",
                table: "ContractPayments",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullyPaidOff",
                table: "ContractPayments");

            migrationBuilder.AddColumn<DateTime>(
                name: "FullPaymnetDate",
                table: "ContractPayments",
                type: "datetime2",
                nullable: true);
        }
    }
}
