﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace apbd_project.Migrations
{
    /// <inheritdoc />
    public partial class AddPaymentsTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PaymentDates",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContractPaymentID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentDates", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PaymentDates_ContractPayments_ContractPaymentID",
                        column: x => x.ContractPaymentID,
                        principalTable: "ContractPayments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentDates_ContractPaymentID",
                table: "PaymentDates",
                column: "ContractPaymentID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentDates");
        }
    }
}
