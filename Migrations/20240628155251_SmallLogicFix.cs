﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace apbd_project.Migrations
{
    /// <inheritdoc />
    public partial class SmallLogicFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "amount",
                table: "PaymentDates",
                type: "decimal(7,2)",
                nullable: false,
                defaultValue: 0m);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "amount",
                table: "PaymentDates");
        }
    }
}
