﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace apbd_project.Migrations
{
    /// <inheritdoc />
    public partial class ChangedDiscountsForeignKey : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiscountSoftwareContract");

            migrationBuilder.AlterColumn<int>(
                name: "SoftwareId",
                table: "SoftwareContracts",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BuyerId",
                table: "SoftwareContracts",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "DiscountSoftware",
                columns: table => new
                {
                    discountsID = table.Column<int>(type: "int", nullable: false),
                    softwareID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountSoftware", x => new { x.discountsID, x.softwareID });
                    table.ForeignKey(
                        name: "FK_DiscountSoftware_Discounts_discountsID",
                        column: x => x.discountsID,
                        principalTable: "Discounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DiscountSoftware_Software_softwareID",
                        column: x => x.softwareID,
                        principalTable: "Software",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiscountSoftware_softwareID",
                table: "DiscountSoftware",
                column: "softwareID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiscountSoftware");

            migrationBuilder.AlterColumn<int>(
                name: "SoftwareId",
                table: "SoftwareContracts",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "BuyerId",
                table: "SoftwareContracts",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "DiscountSoftwareContract",
                columns: table => new
                {
                    contractsID = table.Column<int>(type: "int", nullable: false),
                    discountsID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountSoftwareContract", x => new { x.contractsID, x.discountsID });
                    table.ForeignKey(
                        name: "FK_DiscountSoftwareContract_Discounts_discountsID",
                        column: x => x.discountsID,
                        principalTable: "Discounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DiscountSoftwareContract_SoftwareContracts_contractsID",
                        column: x => x.contractsID,
                        principalTable: "SoftwareContracts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiscountSoftwareContract_discountsID",
                table: "DiscountSoftwareContract",
                column: "discountsID");
        }
    }
}
